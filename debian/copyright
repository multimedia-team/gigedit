Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Gigedit
Upstream-Contact: Christian Schoenebeck <schoenebeck@linuxsampler.org>
 Andreas Persson
Source: https://download.linuxsampler.org/packages/

Files: *
Copyright:
 2006-2020 Andreas Persson
License: GPL-2+

Files:
 debian/*
Copyright:
 2010 Alessio Treglia <alessio@debian.org>
 2008 Free Ekanayaka <freee@debian.org>
 2007 Christian Schoenebeck <cuse@users.sourceforge.net>
 2016-2017, Jaromír Mikeš <mira.mikes@seznam.cz>
 2019 Olivier Humbert <trebmuh@tuxfamily.org>
 2021 Dennis Braun <d_braun@kabelmail.de>
License: GPL-2+

Files: src/gigedit/CombineInstrumentsDialog.cpp
       src/gigedit/CombineInstrumentsDialog.h
       src/gigedit/MacHelper.h
       src/gigedit/MacHelper.mm
       src/gigedit/ReferencesView.cpp
       src/gigedit/ReferencesView.h
       src/gigedit/Settings.cpp
       src/gigedit/Settings.h
       src/gigedit/scripteditor.cpp
       src/gigedit/scripteditor.h
       src/gigedit/scriptslots.cpp
       src/gigedit/scriptslots.h
Copyright: 2013-2020, Christian Schoenebeck
License: GPL-2

Files: osx/autoconf_builder.sh
Copyright: 2007, Toshi Nagata.
License: BSD-2-clause

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

License: GPL-2
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License version 2 as
 published by the Free Software Foundation.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without modification, are permitted 
 provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this list of 
 conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright notice, this list of 
 conditions and the following disclaimer in the documentation and/or other materials provided 
 with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
 PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, 
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF 
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
